export const THREE = require('three')

// Finds the intersection point of two line segments
// defined by (p1, p2) and (p3, p4) respectively
export function lineIntersection(line1, line2) {
    // Intersection offset of the 1st line
    const v1 = line1.start
    const v2 = line1.end
    const v3 = line2.start
    const v4 = line2.end
    const t = ((v3.y-v4.y)*(v1.x-v3.x)+(v4.x-v3.x)*(v1.y-v3.y))/((v4.x-v3.x)*(v1.y-v2.y)-(v1.x-v2.x)*(v4.y-v3.y))
    const vi = new THREE.Vector3()
    line1.at(t, vi)
    return vi
}

// Updates the highlight geometry object
function updateGeometry(i, limit, ring, geometry, highlightLine) {
    let vector = null
    if (i === limit) {
        const start = geometry.vertices[geometry.vertices.length - 1]
        const end = ring.vertices[i]
        const segmentLine = new THREE.Line3(start, end)
        vector = lineIntersection(highlightLine, segmentLine)
    } else {
        vector = ring.vertices[i].clone()
    }
    geometry.vertices.push(vector)
}

// Takes a three.js ring geometry object and return a new geometry 
// representing the highlighted area
export function ringHighlightGeometry(ring, theta) {
    // param ring: the ring geometry object we want to highlight
    // param theta: the amount (in degrees) of the ring we wish to highlight

    // Create a new geometry object 
    const geometry = new THREE.Geometry()

     // Return an empty geometry object in the case of 0 theta
    if (theta === 0) {
        return geometry
    }

    // If we know how many segments make up the ring, we can calculate 
    // how many degrees each segment represents.
    const n = ring.parameters.thetaSegments
    const segmentDelta = 2*Math.PI/n
    const segmentCount = Math.ceil(Math.abs(theta)/segmentDelta)
    
    // Draw an imaginary line from the center to the outer radius of the ring
    // We will use this to highlight only a portion of the last segment conforming
    // to the theta angle
    const r = Math.ceil(ring.parameters.outerRadius)
    const highlightLine = new THREE.Line3(new THREE.Vector3(), new THREE.Vector3(r*Math.cos(theta), r*Math.sin(theta), 0))

    // The segment count represents the number of verticies we 
    // need to clone from the ring geometry.
    geometry.vertices.push(ring.vertices[0].clone())
    if (theta < 0) {
        let i = n - 1
        let limit = (n - segmentCount)
        while (i >= limit) {
            updateGeometry(i, limit, ring, geometry, highlightLine)
            i -= 1
        }
    } else {
        let i = 1
        let limit = segmentCount
        while (i <= limit) {
            updateGeometry(i, limit, ring, geometry, highlightLine)
            i += 1
        }
    }

    // Add the outer ring vertices next
    geometry.vertices.push(ring.vertices[n].clone())
    if (theta < 0) {
        let i = 2*n - 1
        let limit = (2*n - segmentCount)
        while (i >= limit) {
            updateGeometry(i, limit, ring, geometry, highlightLine)
            i -= 1
        }
    } else {
        let i = n + 1
        let limit = (n + segmentCount)
        while (i <= limit) {
            updateGeometry(i, limit, ring, geometry, highlightLine)
            i += 1
        }
    }

    // Need to calculate the faces for the geometry
    // There should be at least 2 per segment
    let i = 0
    let m = geometry.vertices.length/2
    while (i < (m-1)) {
        geometry.faces.push(new THREE.Face3(i, i+1, m+i))
        geometry.faces.push(new THREE.Face3(i+1, m+i+1, m+i))
        i += 1
    }

    return geometry
}